<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Animal;

class AnimalController extends Controller
{
    public function getAnimals(){
        $animales =  Animal::all();
        return response(['data' => $animales], 201);
    }
}
